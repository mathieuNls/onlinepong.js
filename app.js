var app = require('express')()
	, server = require('http').createServer(app)
	, io = require('socket.io').listen(server);

var mySockets = new Array();
var player = new Array();

server.listen(8081);

app.get('/', function handler(req, res){
	res.sendfile(__dirname + '/index.html');
});

app.get('/controls.html', function handler(req, res){
	res.sendfile(__dirname + '/controls.html');
});

app.get('/javascript.js', function handler(req, res){
	res.sendfile(__dirname + '/javascript.js');
});

app.get('/css.css', function handler(req, res){
	res.sendfile(__dirname + '/css.css');
});

app.get('/cursor.jpg', function handler(req, res){
	res.sendfile(__dirname + '/cursor.jpg');
});


io.sockets.on('connection', function (socket) {

  var id = makeid();
  socket.emit('welcome', {message: id});

  player[id] = 0;
  mySockets[id] = io
	  .of('/'+id)
	  .on('connection', function (socket) {
	    
	    player[id] = player[id] + 1;
		
		
		socket.emit('playerNumber', player[id]);

	    socket.on('update', function(Y, X, idPlayer) {
	  	 console.log(Y + ' ' + X);
	  	 socket.broadcast.emit('updateCanvas', X, Y, idPlayer);
	  	});

	  });
	  
});


function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}