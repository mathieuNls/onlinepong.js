// Une application qui require express
// Un server http sur 8081
// Un gestionaire de socket.
// Un tableau de "sockets"

var app = require('express')()
	, server = require('http').createServer(app)
	, io = require('socket.io').listen(server)
	, subDomainsSockets = new Array()
	, player = new Array();

server.listen(8081);

//Serves index.html on '/' calls
app.get('/', function handler(req, res){
	res.sendfile(__dirname + "/index2.html");
});

//Serves onlinePong.js
app.get('/onlinePong.js', function handler(req, res){
	res.sendfile(__dirname + "/onlinePong.js")
});

//Serves control.html
app.get('/controls.html', function handler(req, res){
	res.sendfile(__dirname + "/controls2.html");
});

// La function connection sur le Socket qui
// est invoquée par io.connect chez le client
// Main Domain Socket
io.sockets.on('connection', function(socket){
	// Créer un ID
	var id = makeid();

	// 1
	// Renvoyer l'id à l'utilisateur sur sa méthod 
	// 'welcome'
	socket.emit('welcome', {message:id});

	player[id] = -1;

	// 3
	// Creation d'un tableau de sockets
	// Sous socket qui correspond à l'ID généré
	subDomainsSockets[id] = io.of('/'+id)
		.on('connection', function (socket){

			console.log("Client dans sa room");

			socket.emit('playerNumber', {message:player[id]});

			player[id] = player[id] + 1;

			// Un des clients update sa position
			socket.on('update', function(Y, X, idPlayer){
				console.log("Player " + idPlayer + " mouse at X:" + X + " Y:" + Y);

				// On renvoi à tous.
				socket.broadcast.emit('positionChange', 
					Y, X, idPlayer);
			});

			//Methods pour la prise en charge du jeu
		});

});

//GENERATE RANDOM ID
function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ ){
        text += possible.charAt(Math.floor(Math.random() 
        	* possible.length));
    }

    return text;
}