// SERVEUR INITIALISATION

var app = require('express')()
	, server = require('http').createServer(app)
	, io = require('socket.io').listen(server);

server.listen(8081);

var mySockets = new Array();
var player = new Array();

// ROUTAGE
app.get('/', function handler(req, res){
	res.sendfile(__dirname + '/index.html');
});

app.get('/javascript.js', function handler(req, res){
	res.sendfile(__dirname + "/javascript.js");
});

app.get('/controls.html', function handler(req, res){
	res.sendfile(__dirname + "/controls.html")
});


// MAIN SOCKET
io.sockets.on('connection', function(socket){


	var id = makeid();

	socket.emit('welcome', {message:id});

	player[id] = 0;
	// SUB DOMAIN SOCKET
	mySockets[id] = io.of('/'+id)
	.on('connection', function(socket){

		player[id] = player[id] + 1;
		//Send the id to the new player
		socket.emit('playerNumber', {message:player[id]});

		// Players update their position
		socket.on('update', function(Y, X, idPlayer){
			console.log(Y + ' ' + X + ' ' + idPlayer);

			// We inform the game when they do so.
			socket.broadcast.emit('positionChange', 
				Y, X, idPlayer);
		});

	});

});

//GENERATE RANDOM ID
function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

// node monServer.js
// Browse localhost:8081